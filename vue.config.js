module.exports = {
  publicPath:'/atelier',
  transpileDependencies: [
    'vuetify'
  ],
  devServer: {
    clientLogLevel: 'info',
    proxy: {
      '/api': {
        target: 'http://localhost:8000',
        changeOrigin: true,
      },
    },
  },
}
