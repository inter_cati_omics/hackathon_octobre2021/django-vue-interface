# atelier-vue-2021

Tutorial dans docs/atelierVue2021.pdf

## Liens importants :

Data : https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/django-vue.js

Django Api : https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/django-rest-api


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
