import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const axios = require("axios");


export default new Vuex.Store({
  state: {
    results: [],
    success: true,
    loading: false,
    message: ''
  },
  getters: {
    getMessage: state => {
      return state.message;
    },
    getSuccess: state => {
      return state.success
    },
    getLoading: state => {
      return state.loading
    },
    getResults: state => {
      return state.results
    },
    getIds: state => {
      return state.results.map(r => r.id)
    }
  },
  mutations: {
    setError(state) {
      state.succes = false
    },
    setSuccess(state) {
      state.success= true
    },
    setLoading(state) {
      state.loading = true
    },
    setLoaded(state) {
      state.loading = false
    },
    setResults(state, results) {
      state.results = results
    },
    setMessage(state, message) {
      state.message = message
    }
  },
  actions: {
    fetchResults({ commit }) {
      commit("setLoading")
      axios
      .get("/atelier/data/allOrthomcls.json")
        .then(resp => {
          commit("setSuccess")
          commit("setResults", resp.data)
          commit("setMessage", "Results loaded")
        }, err => {
          commit("setError")
          commit("setMessage", "Error during loading")
          console.error(err)
      })
        .catch(function (error) {
          commit("setMessage", "Error during loading")
          commit("setError")
          console.error(error);
        })
        .finally(() => commit("setLoaded"));
    }
  },
  modules: {
  }
})
